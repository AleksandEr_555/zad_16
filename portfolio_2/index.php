<?php
$conection = new PDO('mysql:host=localhost; dbname=portfolio_db; charset:utf8', 'root', '');

if ($_POST['text'])
{
    $username = htmlspecialchars($_POST['username']);
    $text = htmlspecialchars($_POST['text']);
    $time = date("Y-m-d, H:i:s");
    $safe = $conection->prepare("INSERT INTO comments SET username=:username, comment=:text, date='$time'");
    $arr = ['username'=>$username, 'text'=>$text];
    $safe->execute($arr);
    header('Location: index.php');
}

$aboutData = $conection->query("SELECT * FROM about");
$aboutData = $aboutData->fetch();
$education = $conection->query("SELECT * FROM education");
$languagesData = $conection->query("SELECT * FROM languages");
$interestsData = $conection->query("SELECT * FROM interests ");
$skillsData = $conection->query("SELECT * FROM skills");
?>
<html>
<body style="background-color: LightBlue">
<center>
    <div style='width: 900px; height: 200px; margin-top: 10px; border: 1px solid black; background-image: url("shapka.png")'>
    </div>
    <div style='width: 900px; height: 2500px; margin-top: 10px; border: 1px solid black; background-color: LightGray; text-align: left; padding-top: 0px;'>
        <div style='width: 670px; height: 1200px; margin-top: 0px; margin-left: 0px; background-color: white; padding-left: 20px'>
            <br>
            <h1>Карьера</h1>
            <h3>Пока ещё только учусь. ;)</h3>
            <hr>
            <br>
            <h1>Опыт паботы</h1>
            <h3>Опыта работы нет.</h3>
            <hr>
            <br>
            <h1>Пректы</h1>
            <h3>Ещё не учавствовал в каких-либо проектах.</h3>
            <hr>
            <br>
            <h1>Навыки</h1>
            <?php foreach ($skillsData as $skills) { ?>
                <p><h4 style="margin-top:5px; color: DimGray"><?=$skills['title']?></h4> <b> - <?=$skills['level']?></b></p><br>
            <?php } ?>
            <hr>
        </div>
        <div style='width: 210px; height: 280px; margin-top: -1200px;margin-left: 690px; background-color: SteelBlue'>
            <center>
                <div style='width: 90px; height: 90px'>
                    <img src="cat.png" style="margin-top: 30px">
                </div>
            </center>

            <h1 align="center" style="margin-top: 40px; color: white"><?=$aboutData['name']?></h1>
            <h3 align="center"><b>Статус: <?=$aboutData['post']?></b></h3>

        </div>

        <div style='width: 200px; height: 920px; margin-left: 690px; background-color: CornflowerBlue; text-align: left; padding-left: 10px'>
            <br>
            <h5>Почта: <a href="https://<?= $aboutData['email']?>"><?= $aboutData['email']?></a></h5>
            <h5>Телефон: <?= $aboutData['phone']?></h5>
            <hr>
            <br>
            <h2 style="color: white">Образование</h2>
            <?php foreach ($education as $value) { ?>
                <h4 style="margin-top:5px; color: Gainsboro"><?=$value['faculty']?></h4>
                <h5 style="margin-top:5px"><?=$value['university']?></h5>
                <p><?=$value['yearStart']?> - <?=$value['yearEnd']?></p>
            <?php } ?>
            <hr>
            <br>
            <h2 style="color: white">Языки</h2>
            <?php foreach ($languagesData as $languages) { ?>
                <h4 style="margin-top:5px; color: Gainsboro"><?=$languages['title']?></h4>
            <?php } ?>
            <hr>
            <br>
            <h2 style="color: white">Интересы</h2>
            <?php foreach ($interestsData as $interests) { ?>
                <h4 style="margin-top:5px; color: Gainsboro"><?=$interests['name']?></h4>
            <?php } ?>
        </div>
        <div style='width: 890px; height: 1300px; margin-left: 0px; background-color: DarkGray; text-align: left; padding-left: 10px'>
            <br>
            <h2>Коментарии</h2>
            <hr>
            <form action="" method="POST">
                <input type="text" name="username" required placeholder="Ваше имя">
                <br>
                <textarea type="text" name="text" required placeholder="Ваше сообщение" cols="30" row="5"></textarea>
                <button>Отправить</button>
            </form>
            <hr>
            <?php
            $data = $conection->query("SELECT * FROM comments");
            foreach ($data as $value)
            {
                echo $value['date'] . ' ' . $value['username'] . ' написал: ' . $value['comment'] . "<hr>";
            }
            ?>
        </div>
    </div>
</center>
</body>
</html>